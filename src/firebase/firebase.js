import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const prodConfig = {
  apiKey: "AIzaSyAaDWLhEfIDzwXMh4UoQUICXfYfbJYXMwU",
  authDomain: "crud-804d5.firebaseapp.com",
  databaseURL: "https://crud-804d5.firebaseio.com",
  projectId: "crud-804d5",
  storageBucket: "crud-804d5.appspot.com",
  messagingSenderId: "14021351710"
};

const devConfig = {
  apiKey: "AIzaSyAaDWLhEfIDzwXMh4UoQUICXfYfbJYXMwU",
  authDomain: "crud-804d5.firebaseapp.com",
  databaseURL: "https://crud-804d5.firebaseio.com",
  projectId: "crud-804d5",
  storageBucket: "crud-804d5.appspot.com",
  messagingSenderId: "14021351710"
};

const config = process.env.NODE_ENV === 'production'
  ? prodConfig
  : devConfig;

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();

export {
  db,
  auth,
};
