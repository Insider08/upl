export const MATCHES = '/';
export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const PASSWORD_FORGET = '/pw-forget';
export const LIVE = '/live';
export const ACCOUNT = '/account';
