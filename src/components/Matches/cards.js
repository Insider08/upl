import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const CARD_QUERY = 'https://api.fifa.com/api/v1/calendar/matches?idseason=2000011094&idcompetition=2000000043&language=en-GB&count=1000';

class Cards extends Component{
    constructor(props) {
        super(props);
    
        this.state = {
          results: []
        };
      }

    fetchData() {
        fetch(CARD_QUERY)
        .then(response => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error('Something went wrong ...');
        }
        })
        .then(data => this.setState({ results: data.Results, isLoading: true }))
        .catch(error => this.setState({ error, isLoading: true }));
    }
    componentDidMount() {
    this.fetchData()
    }

    render(){
    const { results } = this.state;
    let mStatus = '';
    let getMatchStatus = (statusNum) => {
    switch(statusNum) {
        case 0:
            mStatus = 'Finished'
            break;
        case 1:
            mStatus = 'To be played'
            break;
        case 3:
            mStatus = 'Live'
            break;
        case 12:
            mStatus = 'Line ups'
            break;
        case 4:
            mStatus = 'Abandoned'
            break;
        case 7:
            mStatus = 'Postponed'
            break;
        case 8:
            mStatus = 'Cancelled'
            break;
        case 99:
            mStatus = 'Suspended'
            break;
        default:
            mStatus = ''
        }
        return mStatus;
    }
    return(

    results.map(result =>
        
        <Card key={result.IdMatch} id={result.MatchDay} className="matches_cards">
        <Grid container>
        <Grid item xs={4} sm={5}>
        <Typography variant="subheading" gutterBottom align="right">
        <img alt={result.Home.TeamName[0].Description} src={require('./team-logos/' + result.Home.IdTeam + 'x3.png')} /><br/>
        {result.Home.TeamName[0].Description}
        </Typography>
        </Grid>
        <Grid item xs={4} sm={2}>
        <Typography variant="caption" gutterBottom align="center">
        Matchday: {result.MatchDay}<br/>
        {getMatchStatus(result.MatchStatus)}
        </Typography>
        <Typography variant="display1" gutterBottom align="center">
        {result.HomeTeamScore} : {result.AwayTeamScore}
        </Typography>
        <Typography variant="caption" gutterBottom align="center">
        <p>{result.LocalDate.slice(0,10)}<br/>
        {result.LocalDate.slice(11,16)}</p>
        </Typography>
        </Grid>
        <Grid item xs={4} sm={5}>
        <Typography variant="subheading" gutterBottom align="left">
        <img alt={result.Away.TeamName[0].Description} src={require('./team-logos/' + result.Away.IdTeam + 'x3.png')} /><br/>
        {result.Away.TeamName[0].Description}
        </Typography>
        </Grid> 
        </Grid>
        </Card>
        ))
        }
    }
export default Cards;