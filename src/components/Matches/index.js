import React from 'react';
import Cards from './cards';
import './index.css';
import FloatButton from './button';
import Typography from '@material-ui/core/Typography';


const MatchesPage = () =>
      <div>
      <Typography variant="display1" gutterBottom align="center">
        UPL Calendar
      </Typography>
        <Cards />
        <FloatButton/>
      </div>

export default MatchesPage;
