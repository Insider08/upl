import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import RotateRightRounded from '@material-ui/icons/RotateRightRounded';


const NEXT_QUERY = 'https://api.fifa.com/api/v1/calendar/nextmatches?numberOfNextMatches=1&numberOfPreviousMatches=0&idseason=2000011094&idcompetition=2000000043'

class FloatButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      matchDay: ''
    };
  }
  getCurrentMatchDay() {
    fetch(NEXT_QUERY)
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Something went wrong ...');
      }
    })
    .then(data => this.setState({ matchDay: data.Results[0].MatchDay, isLoading: true }))
    .catch(error => this.setState({ error, isLoading: true }));
  }

  componentDidMount() {
    this.getCurrentMatchDay()
  }
  render() {
    const { matchDay } = this.state;
    return (
      <div>
        <Button className='float-btn' variant="extendedFab" href={'/#' + matchDay} aria-label="Delete" >
        <RotateRightRounded />
        Upcoming
        </Button>
      </div>
    );
  }
}

export default FloatButton;