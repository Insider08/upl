import React from 'react';

import { auth } from '../../firebase';
import MenuItem from '@material-ui/core/MenuItem';

const SignOutButton = () =>

  <MenuItem
    className="light-nav"
    onClick={auth.doSignOut}
  >
    Sign Out
    </MenuItem>

export default SignOutButton;
