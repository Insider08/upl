import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import { SignUpLink } from '../SignUp';
import { PasswordForgetLink } from '../PasswordForget';
import { auth } from '../../firebase';
import * as routes from '../../constants/routes';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const SignInPage = ({ history }) =>
  <div>
    <Typography variant="display1" gutterBottom align="center">
      Sign In
    </Typography>
    <br/>
    <SignInForm history={history} />
    <br/>
    <PasswordForgetLink /> 
    <SignUpLink />
  </div>

const updateByPropertyName = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

class SignInForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = (event) => {
    const {
      email,
      password,
    } = this.state;

    const {
      history,
    } = this.props;

    auth.doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
        history.push(routes.LIVE);
      })
      .catch(error => {
        this.setState(updateByPropertyName('error', error));
      });

    event.preventDefault();
  }

  render() {
    const {
      email,
      password,
      error,
    } = this.state;

    const isInvalid =
      password === '' ||
      email === '';

    return (
      <form onSubmit={this.onSubmit} align="center">
          <br/>
        <TextField
          id="email"
          value={email}
          onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
          label="Email"
          type="email"
          autoComplete="current-password"
          margin="normal"
        />
        <TextField
          id="password"
          value={password}
          onChange={event => this.setState(updateByPropertyName('password', event.target.value))}
          label="Password"
          type="password"
          autoComplete="current-password"
          margin="normal"
        />
        <br/>
        <Button variant="outlined" color="primary" disabled={isInvalid} type="submit">
          Sign In
        </Button>

        { error && <p>{error.message}</p> }
      </form>
    );
  }
}

export default withRouter(SignInPage);

export {
  SignInForm,
};
