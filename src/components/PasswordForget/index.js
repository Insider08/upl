import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { auth } from '../../firebase';
import * as routes from '../../constants/routes';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const PasswordForgetPage = () =>
  <div>
    <Typography variant="display1" gutterBottom align="center">
      Forgot Password?
    </Typography>
    <br/>
    <PasswordForgetForm />
  </div>

const updateByPropertyName = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  email: '',
  error: null,
};

class PasswordForgetForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = (event) => {
    const { email } = this.state;

    auth.doPasswordReset(email)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
      })
      .catch(error => {
        this.setState(updateByPropertyName('error', error));
      });

    event.preventDefault();
  }

  render() {
    const {
      email,
      error,
    } = this.state;

    const isInvalid = email === '';

    return (
      <form onSubmit={this.onSubmit} align="center">
        <TextField
          id="email"
          value={this.state.email}
          onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
          label="Email"
          type="email"
          autoComplete="email"
          margin="normal"
        />
        <br/>
        <Button variant="outlined" color="primary" disabled={isInvalid} type="submit">
          Reset My Password
        </Button>

        { error && <p>{error.message}</p> }
      </form>
    );
  }
}

const PasswordForgetLink = () =>
<Typography variant="caption" gutterBottom align="center">
  <Link to={routes.PASSWORD_FORGET}>Forgot Password?</Link>
</Typography>

export default PasswordForgetPage;

export {
  PasswordForgetForm,
  PasswordForgetLink,
};
