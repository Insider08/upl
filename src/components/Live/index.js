import React from 'react';
import LiveCards from './cards';
import Typography from '@material-ui/core/Typography';


const LivePage = () =>
      <div>
      <Typography variant="display1" gutterBottom align="center">
        Live Matches
      </Typography>
        <LiveCards />
      </div>

export default LivePage;
