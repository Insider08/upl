import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import '../Matches/index.css'

const LIVE_QUERY = 'https://api.fifa.com/api/v1/live/football/recent/2000000043/2000011094?language=en-GB';

class LiveCards extends Component{
    constructor(props) {
        super(props);
    
        this.state = {
          liveResults: []
        };
      }

    fetchData() {
        fetch(LIVE_QUERY)
        .then(response => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error('Something went wrong ...');
        }
        })
        .then(data => this.setState({ liveResults: data.Results, isLoading: true }))
        .catch(error => this.setState({ error, isLoading: true }));
    }
    componentDidMount() {
      this.fetchData();
      this.interval = setInterval(() => this.setState({ time: Date.now() }), 60000);
      console.log('Updated')
    }

    componentWillUnmount() {
      clearInterval(this.interval);
    }

    render(){
    const { liveResults } = this.state;
    let pStatus = '';
    let getMatchStatus = (statusNum) => {
    switch(statusNum) {
        case 1:
            pStatus = 'Scheduled'
            break;
        case 2:
            pStatus = 'PreMatch'
            break;
        case 3:
            pStatus = 'First Half'
            break;
        case 4:
            pStatus = 'Half Time'
            break;
        case 5:
            pStatus = 'Second Half'
            break;
        case 6:
            pStatus = 'Extra Time'
            break;
        case 7:
            pStatus = 'Extra First Half'
            break;
        case 8:
            pStatus = 'Extra Half Time'
            break;
        case 9:
            pStatus = 'Extra Second Half'
            break;
        case 10:
            pStatus = 'Full Time'
            break;
        case 11:
            pStatus = 'Penalty Shootout'
            break;
        default:
            pStatus = ''
        }
        return pStatus;
    }


    if(liveResults.IdMatch !== undefined){
    return(
      
      liveResults.map(result =>
        
      <Card key={result.IdMatch} id={result.MatchDay} className="matches_cards">
        <Grid container>
          <Grid item xs={4} sm={5}>
            <Typography variant="subheading" gutterBottom align="right">
              <img alt={result.Home.TeamName[0].Description} src={require('../Matches/team-logos/' + result.Home.IdTeam + 'x3.png')} /><br/>
              {result.Home.TeamName[0].Description}
            </Typography>
          </Grid>
          <Grid item xs={4} sm={2}>
            <Typography variant="caption" gutterBottom align="center">
              Matchday: {result.MatchDay}<br/>
              {getMatchStatus(result.Period)}
            </Typography>
            <Typography variant="display1" gutterBottom align="center">
              {result.HomeTeamScore} : {result.AwayTeamScore}
            </Typography>
            <Typography variant="caption" gutterBottom align="center">
              <p>{result.LocalDate.slice(0,10)}<br/>
              {result.LocalDate.slice(11,16)}</p>
            </Typography>
          </Grid>
          <Grid item xs={4} sm={5}>
            <Typography variant="subheading" gutterBottom align="left">
              <img alt={result.Away.TeamName[0].Description} src={require('../Matches/team-logos/' + result.Away.IdTeam + 'x3.png')} /><br/>
              {result.Away.TeamName[0].Description}
            </Typography>
          </Grid> 
        </Grid>
      </Card>
      ))
        }else {
          return(
            <Typography variant="subheading" gutterBottom align="center">
              There Are No Live Matches Now
              {console.log('Updated')}
            </Typography>
          )
        }
    }
  } 
export default LiveCards;