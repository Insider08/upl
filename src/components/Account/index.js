import React from 'react';
import { inject, observer } from 'mobx-react';
import { compose } from 'recompose';
import PasswordChangeForm from '../PasswordChange';
import withAuthorization from '../Session/withAuthorization';
import Typography from '@material-ui/core/Typography';

const AccountPage = ({ sessionStore }) =>
  <div>
    <Typography variant="display1" gutterBottom align="center">
      Account: {sessionStore.authUser.email}
    </Typography>
    <PasswordChangeForm />
  </div>

const authCondition = (authUser) => !!authUser;

export default compose(
  withAuthorization(authCondition),
  inject('sessionStore'),
  observer
)(AccountPage);