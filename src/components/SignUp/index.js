import React, { Component } from 'react';
import {
  Link,
  withRouter,
} from 'react-router-dom';

import { auth, db } from '../../firebase';
import * as routes from '../../constants/routes';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const SignUpPage = ({ history }) =>
  <div>
    <Typography variant="display1" gutterBottom align="center">
      Sign Up
    </Typography>
    <br/>
    <SignUpForm history={history} />
  </div>

const updateByPropertyName = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  username: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
};

class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = (event) => {
    const {
      username,
      email,
      passwordOne,
    } = this.state;

    const {
      history,
    } = this.props;

    auth.doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {

        // Create a user in your own accessible Firebase Database too
        db.doCreateUser(authUser.user.uid, username, email)
          .then(() => {
            this.setState(() => ({ ...INITIAL_STATE }));
            history.push(routes.LIVE);
          })
          .catch(error => {
            this.setState(updateByPropertyName('error', error));
          });

      })
      .catch(error => {
        this.setState(updateByPropertyName('error', error));
      });

    event.preventDefault();
  }

  render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      error,
    } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      username === '' ||
      email === '';

    return (
      <form onSubmit={this.onSubmit} align="center">
        <TextField
          id="username"
          value={username}
          onChange={event => this.setState(updateByPropertyName('username', event.target.value))}
          label="Username"
          type="text"
          autoComplete="username"
          margin="normal"
        />
        <TextField
          id="email"
          value={email}
          onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
          label="Email"
          type="email"
          autoComplete="email"
          margin="normal"
        />
        <TextField
          id="password-one"
          value={passwordOne}
          onChange={event => this.setState(updateByPropertyName('passwordOne', event.target.value))}
          label="Password"
          type="password"
          autoComplete="password"
          margin="normal"
        />
        <TextField
          id="password-two"
          value={passwordTwo}
          onChange={event => this.setState(updateByPropertyName('passwordTwo', event.target.value))}
          label="Confirm Password"
          type="password"
          autoComplete="password"
          margin="normal"
        />
        
        <br/>
        <Button variant="outlined" color="primary" disabled={isInvalid} type="submit">
          Sign Up
        </Button>

        { error && <p>{error.message}</p> }
      </form>
    );
  }
}

const SignUpLink = () =>
  <Typography variant="caption" gutterBottom align="center">
  Don't have an account?  <Link to={routes.SIGN_UP}>Sign Up</Link>
  </Typography>

export default withRouter(SignUpPage);

export {
  SignUpForm,
  SignUpLink,
};