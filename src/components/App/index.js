import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';

import Navbar from '../Navbar';
import MatchesPage from '../Matches';
import SignUpPage from '../SignUp';
import SignInPage from '../SignIn';
import PasswordForgetPage from '../PasswordForget';
import LivePage from '../Live';
import AccountPage from '../Account';
import withAuthentication from '../Session/withAuthentication';
import * as routes from '../../constants/routes';

import './index.css';

const App = () =>
  <Router>
    <div className="app">
    
      <Navbar />
      <div className="page">
      <Route exact path={routes.MATCHES} component={() => <MatchesPage />} />
      <Route exact path={routes.SIGN_UP} component={() => <SignUpPage />} />
      <Route exact path={routes.SIGN_IN} component={() => <SignInPage />} />
      <Route exact path={routes.PASSWORD_FORGET} component={() => <PasswordForgetPage />} />
      <Route exact path={routes.LIVE} component={() => <LivePage />} />
      <Route exact path={routes.ACCOUNT} component={() => <AccountPage />} />
      </div>
    </div>
  </Router>

export default withAuthentication(App);