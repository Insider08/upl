import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Navigation from '../Navigation'

class ButtonAppBar extends React.Component{
render(){
  return (
    <div >
      <AppBar position="fixed" className="appbar">
        <Toolbar>
          <Typography variant="title" color="inherit" >
            UPL 
          </Typography>
          <Navigation/>
        </Toolbar>
      </AppBar>
    </div>
  )}
}


export default ButtonAppBar;