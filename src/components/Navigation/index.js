import React from 'react';

import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import { compose } from 'recompose';
import SignOutButton from '../SignOut';
import * as routes from '../../constants/routes';
import './index.css';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';

const Navigation = ({ sessionStore }) =>
  <div className="appNav">
    { sessionStore.authUser
        ? <NavigationAuth />
        : <NavigationNonAuth />
    }
    
  </div>

const NavigationAuth = () => 
    <MenuList >
      <MenuItem className="light-nav" component={Link} to={routes.MATCHES}>
      Calendar</MenuItem>
      <MenuItem className="light-nav" component={Link} to={routes.LIVE}>
      Live</MenuItem>
      <MenuItem className="light-nav" component={Link} to={routes.ACCOUNT}>
      Account</MenuItem>
      <SignOutButton />
    </MenuList>

const NavigationNonAuth = () =>
  <MenuList >
    <MenuItem className="light-nav" selected component={Link} to={routes.MATCHES}>
    Calendar</MenuItem>
    <MenuItem className="light-nav" component={Link} to={routes.SIGN_IN}>
    Sign In</MenuItem>
  </MenuList>


export default compose(
  inject('sessionStore'),
  observer
)(Navigation);
