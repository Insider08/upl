## Features

* uses:
  * React (create-react-app)
  * firebase 5.0.0
  * react-router 4.2.0
  * mobX
* features:
  * Sign In
  * Sign Up
  * Sign Out
  * Password Forget
  * Password Change
  * Protected Routes with Authorization
  * Database: Users

## Installation

* `git clone https://Insider08@bitbucket.org/Insider08/upl.git`
* `cd upl`
* `npm install`
* `npm start`
* visit http://localhost:3000/
* Use your own Firebase Credentials
